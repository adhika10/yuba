<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>Add Customer</title>
<link rel="shortcut icon" type="image/png" href="favicon.png">

<style>
.middle {
	display: block;
	margin-left: auto;
	margin-right: auto;
	width: 50%;
}

.form {
	width: 350px;
}

label {
	float: left;
	width: 130px;
}

select {
	float: left;
	width: 168px;
}

input[type=text] {
	float: left;
	width: 168px;
}

.clear {
	clear: both;
	height: 0;
	line-height: 0;
}

.floatright {
	float: right;
}

input[type=submit] {
	width: 120px;
	height: 30px
}
</style>

</head>

<body style="background-color: #1B93CF">

	<h3 style="text-align: center">
		<b> Welcome to Yes-Bank</b>
	</h3>

	<a href="http://localhost:8080/"><img src="Yes_Bank.png"
		height="150" width="150" class="middle"></a>

	<h3 style="text-align: center">
		<b><a href="home.jsp"> Back to home</a></b>
	</h3>

	<hr>


	<h1>Add Customer</h1>
	<form class="form" action="/add" modelAttribute = "customer">
		<label>First Name: </label> <input type="text" name="firstName"
			placeholder="Enter your First Name" required /> <br> <br>
		<label>Last Name: </label> <input type="text" name="lastName"
			placeholder="Enter your last Name" required /> <br> <br> <label>Address:</label>
		<input type="text" name="Address" placeholder="Enter your address"
			required /> <br> <br> <label>ZipCode:</label><input
			type="text" name="zipCode" pattern="[0-9]{5}" placeholder="ZipCode" />
		<br> <br> <label>SocialSecurity:</label> <input type="text"
			name="socialSecurity" pattern="[0-9]{9}" placeholder="SSN" required />
		<br> <br> <label>Select State:</label> <select id="State"
			name="state" placeholder="OH">
			<option value="AL">Alabama</option>
			<option value="AK">Alaska</option>
			<option value="AZ">Arizona</option>
			<option value="AR">Arkansas</option>
			<option value="CA">California</option>
			<option value="CO">Colorado</option>
			<option value="CT">Connecticut</option>
			<option value="DE">Delaware</option>
			<option value="DC">District Of Columbia</option>
			<option value="FL">Florida</option>
			<option value="GA">Georgia</option>
			<option value="HI">Hawaii</option>
			<option value="ID">Idaho</option>
			<option value="IL">Illinois</option>
			<option value="IN">Indiana</option>
			<option value="IA">Iowa</option>
			<option value="KS">Kansas</option>
			<option value="KY">Kentucky</option>
			<option value="LA">Louisiana</option>
			<option value="ME">Maine</option>
			<option value="MD">Maryland</option>
			<option value="MA">Massachusetts</option>
			<option value="MI">Michigan</option>
			<option value="MN">Minnesota</option>
			<option value="MS">Mississippi</option>
			<option value="MO">Missouri</option>
			<option value="MT">Montana</option>
			<option value="NE">Nebraska</option>
			<option value="NV">Nevada</option>
			<option value="NH">New Hampshire</option>
			<option value="NJ">New Jersey</option>
			<option value="NM">New Mexico</option>
			<option value="NY">New York</option>
			<option value="NC">North Carolina</option>
			<option value="ND">North Dakota</option>
			<option value="OH">Ohio</option>
			<option value="OK">Oklahoma</option>
			<option value="OR">Oregon</option>
			<option value="PA">Pennsylvania</option>
			<option value="RI">Rhode Island</option>
			<option value="SC">South Carolina</option>
			<option value="SD">South Dakota</option>
			<option value="TN">Tennessee</option>
			<option value="TX">Texas</option>
			<option value="UT">Utah</option>
			<option value="VT">Vermont</option>
			<option value="VA">Virginia</option>
			<option value="WA">Washington</option>
			<option value="WV">West Virginia</option>
			<option value="WI">Wisconsin</option>
			<option value="WY">Wyoming</option>

		</select> <br> <br> <input style="background-color: lightblue"
			type="submit" value="Add Customer" class="shift" />
	</form>