<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">

<title>Customer List</title>
<link rel="shortcut icon" type="image/png" href="favicon.png">

<style>
.middle {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;
}

.form { width: 350px; }
		label { float: left; width: 130px; }
		select {float: left; width: 168px; }

		input[type=text] { float: left; width: 168px; }

		.clear { clear: both; height: 0; line-height: 0; }

		.floatright { float: right; }
		
input[type=submit] {
		width: 120px; height: 30px
}

</style>

</head>

<body style="background-color: #1B93CF">

<h3 style=text-align:center><b> Welcome to Yes-Bank</b></h3>
	
	<a href="http://localhost:8080/"><img src="Yes_Bank.png" height="150" width="150" class="middle" ></a>
		
		<h3 style=text-align:center> <b><a href = "home.jsp"> Back to home</a></b></h3>
		
		<hr>

	<h1>Customer List</h1>
	<table border="1">
		<tr>
			<th>#</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Address</th>
			<th>Modify</th>
		</tr>
		
		<%! int data=1; %> 
		
		<c:forEach items="${customers}" var="cusList">
			<tr>
				<td>${data=data+1}</td>
				<td>${cusList.firstName}</td>
				<td>${cusList.lastName}</td>
				<td>${cusList.address} ${cusList.state} ${cusList.zipCode}</td>
				<td> 
					<a href="${pageContext.request.contextPath}/deleteById/${cusList.id}" 
					onclick = "return confirm('You sure you want to delete this information?')">
					<input type="button" value = "Remove"/></a></td>
				
			</tr>


		</c:forEach>

	</table>


</body>
</html>