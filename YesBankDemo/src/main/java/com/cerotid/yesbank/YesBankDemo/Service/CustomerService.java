package com.cerotid.yesbank.YesBankDemo.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cerotid.yesbank.YesBankDemo.Entity.Customer;
import com.cerotid.yesbank.YesBankDemo.Repository.CustomerRepository;

@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;
	
	public Customer addToDB (Customer customer) {
		customerRepository.save(customer);
		return customer;
	}
	
	public List<Customer> allCustomer(){
		return customerRepository.findAll();
	}
	
	public void delete (int id) {
		customerRepository.deleteById(id);
	}
	
	

}
