package com.cerotid.yesbank.YesBankDemo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cerotid.yesbank.YesBankDemo.Entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer>{

}
