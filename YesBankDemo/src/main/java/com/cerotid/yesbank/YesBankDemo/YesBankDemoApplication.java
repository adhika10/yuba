package com.cerotid.yesbank.YesBankDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YesBankDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(YesBankDemoApplication.class, args);
	}

}
