package com.cerotid.yesbank.YesBankDemo.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cerotid.yesbank.YesBankDemo.Entity.Customer;
import com.cerotid.yesbank.YesBankDemo.Service.CustomerService;

@Controller 
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@GetMapping("/")
	public String homepage() {
		return "home";
	}
	
	@GetMapping("/addcustomer")
	public String custumerPage(Model model) {
		model.addAttribute("customer", new Customer());
		
		return "addcustomer";
		
	}
	
	@GetMapping("/add")
	public String addCustomer (@ModelAttribute("customer") Customer customer, Model model) {
		customerService.addToDB(customer);
		List<Customer> custList = customerService.allCustomer();
		model.addAttribute("customers", custList);
		
		return "customerList";
	}

	@RequestMapping("/cusList")
	public String allCustomerData(Model model) {
		List<Customer> custList = customerService.allCustomer();
		model.addAttribute("customers", custList);
		return "customerList";
		
	}
	
	@RequestMapping("deleteById/{id}")
	public String remove (@PathVariable("id") int id) {
		customerService.delete(id);
		return "redirect:/cusList";
	}	
}
